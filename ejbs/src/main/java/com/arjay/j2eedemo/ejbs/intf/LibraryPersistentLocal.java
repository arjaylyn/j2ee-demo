package com.arjay.j2eedemo.ejbs.intf;

import java.util.List;

import javax.ejb.Local;

import com.arjay.j2eedemo.ejbs.model.Book;

@Local
public interface LibraryPersistentLocal {
    
   public void addBook(Book book);
   public List<Book> getBooks();
}
