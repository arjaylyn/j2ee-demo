package com.arjay.j2eedemo.ejbs.impl;

import java.util.List;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.arjay.j2eedemo.ejbs.intf.LibraryPersistentLocal;
import com.arjay.j2eedemo.ejbs.intf.LibraryPersistentRemote;
import com.arjay.j2eedemo.ejbs.model.Book;

@Stateless
@Remote(LibraryPersistentRemote.class)
@Local(LibraryPersistentLocal.class)
public class LibraryPersistentBean implements LibraryPersistentRemote, LibraryPersistentLocal {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public void addBook(Book book) {
        entityManager.persist(book);
    }

    @Override
    public List<Book> getBooks() {
        return entityManager.createQuery("from Book", Book.class).getResultList();
    }
}
