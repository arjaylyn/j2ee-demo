package com.arjay.j2eedemo.ejbs.intf;

import java.util.List;

import javax.ejb.Remote;

import com.arjay.j2eedemo.ejbs.model.Book;

@Remote
public interface  LibraryPersistentRemote {
    
   public List<Book> getBooks();
}
