package com.arjay.j2eedemo.ejbs.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="books")
public class Book implements Serializable {
    private static final long serialVersionUID = 7511958877335024482L;
    
    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String title;

    private String author;
}
