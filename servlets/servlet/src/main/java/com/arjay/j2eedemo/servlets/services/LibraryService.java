package com.arjay.j2eedemo.servlets.services;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.arjay.j2eedemo.ejbs.intf.LibraryPersistentLocal;
import com.arjay.j2eedemo.ejbs.model.Book;

import org.jboss.resteasy.annotations.providers.jaxb.json.BadgerFish;

@Path("/library/books")
public class LibraryService implements Serializable  {
    private static final long serialVersionUID = 4846144137708847893L;

    @Inject
    LibraryPersistentLocal libraryBean;

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public List<Book> getBooks() {
        return libraryBean.getBooks();
	}
	
	@POST
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public void createBooks(Book book) {
        libraryBean.addBook(book);
	}
}
