package com.arjay.j2eedemo.servlets;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Application;

import com.arjay.j2eedemo.servlets.services.LibraryService;

public class MessageApplication extends Application {
	private HashSet<Class<?>> set = new HashSet<Class<?>>();

	public MessageApplication() {
		getClasses().add(LibraryService.class);
	}

	@Override
	public Set<Class<?>> getClasses() {
		return set;
	}
}
